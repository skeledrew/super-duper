"""Test duper."""


from contextlib import nullcontext as does_not_raise

import pytest


class DemoBase:
    def __init__(self):
        self._w = False
        self._x = 42
        self._y = False
        return

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value
        return

    def w(self):
        return self._w


@pytest.mark.parametrize("super", [super, "duper"])
@pytest.mark.parametrize(
    "args, expect, check",
    [
        ((int,), None, None),
        ((int, int), None, None),
        ((int, 0), None, None),
        (
            (0,),
            pytest.raises(TypeError, match=r"argument 1 must be type, not int"),
            None,
        ),
        (
            (int, "0"),
            pytest.raises(
                TypeError,
                match="obj must be an instance or subtype of type",
            ),
            None,
        ),
        (
            (int, str),
            pytest.raises(
                TypeError,
                match="obj must be an instance or subtype of type",
            ),
            None,
        ),
    ],
    ids=[
        "1type",
        "good_types",
        "type_good_obj",
        "bad_1obj",
        "type_bad_obj",
        "type_bad_type",
    ],
)
def test_duper_sanity(super, args, expect, check):
    """Ensure original behavior is maintained."""
    if super == "duper":
        from super_duper import duper as super

    if not expect:
        expect = does_not_raise()

    if not check:
        check = lambda so: True

    with expect:
        assert check(super(*args))
    return


@pytest.mark.parametrize("scope", ["builtins", "globals"])
def test_duper_no_args(scope):
    """Ensure duper is drop-in compatible.

    NOTE: these are the original duper tests.
    """
    from super_duper import duper, install, uninstall

    if scope == "builtins":
        install()

    elif scope == "globals":
        global super

        super = duper
    accessed = ""

    class A:
        def __init__(self):
            self.value = 0
            return

        @property
        def go(self):
            nonlocal accessed
            accessed += "A"
            return self.value

        @go.setter
        def go(self, value):
            nonlocal accessed
            accessed += "A"
            self.value = value

    class B(A):
        pass

    class C(B):
        @property
        def go(self):
            nonlocal accessed
            accessed += "C"
            return super().go

        @go.setter
        def go(self, value):
            nonlocal accessed
            accessed += "C"
            super().go = value

    class D(B):
        @property
        def go(self):
            nonlocal accessed
            accessed += "D"
            return super().go

        @go.setter
        def go(self, value):
            nonlocal accessed
            accessed += "D"
            super().go = value

    class E(C, D):
        @property
        def go(self):
            nonlocal accessed
            accessed += "E"
            return super().go

        @go.setter
        def go(self, value):
            nonlocal accessed
            accessed += "E"
            super().go = value

    try:
        e = E()
        print("Getting initial value...")
        print("Value:", e.go)
        print("Access order:", accessed)
        tmp = accessed
        accessed = ""
        print("\nSetting new value to 1...")
        e.go = 1
        print("Access order:", accessed)
        assert tmp == accessed
        assert accessed == "ECDA"
        print("Getting final value...")
        print("Value:", e.go)

    finally:
        uninstall()
    return


@pytest.mark.parametrize("super", [super, "duper"])
def test_duper_get(super):
    """Ensure getting is the same.

    NOTE: this might be redundant, given sanity checks.
    """
    if super == "duper":
        from super_duper import duper as super

    class Demo(DemoBase):
        @property
        def x(self):
            return super().x * 2

    assert Demo().x == 84
    return


@pytest.mark.parametrize(
    "super, expect, check",
    [
        (super, pytest.raises(AttributeError), True),
        ("duper", does_not_raise(), True),
    ],
)
def test_duper_set(super, expect, check):
    """The main issue fixed by duper."""
    if super == "duper":
        from super_duper import duper as super

    class Demo(DemoBase):
        @property
        def x(self):
            return super().x * 2

        @x.setter
        def x(self, val):
            self._y = True
            super().x = val
            self._w = True
            return

        def w(self):
            return not super().w()

    with expect:
        demo = Demo()
        assert demo.w() is True
        demo.x = 30
    assert demo._y == check
    return


@pytest.mark.parametrize("super", [super, "duper"])
@pytest.mark.parametrize(
    "make_args",
    [
        (lambda self: ()),
        # (lambda self: (self.__class__,)),  # NOTE: 1arg unsupported
        (lambda self: (type(self), self)),
    ],
    ids=[
        "0arg",
        # "1arg",
        "2arg",
    ],
)
def test_duper_init_args(super, make_args):
    """Target class passes args to it's __init__."""
    if super == "duper":
        from super_duper import duper as super

    class InitArgs:
        def __init__(self, arg1, arg2, a_kwd=None, **rest):
            return

    class InitNoArgs(InitArgs):
        def __init__(self):
            super(*make_args(self)).__init__(42, 13, more=False)
            return

    assert InitNoArgs()
    return


@pytest.mark.parametrize(
    "super, expect",
    [
        (super, pytest.raises(AttributeError)),
        (
            "duper",
            pytest.raises(
                AttributeError,
                match="'super' object has no attribute '_super'",
            ),  # see docstring note
        ),
    ],
)
def test_duper_attr_collision(super, expect):
    """Ensure there's no collision between duper's and the target's attrs.

    Also tests for setting a non-callable base attr, on which super also fails.

    FIXME: the duper error is currently ignored as it breaks/complicates
            the *_no_args fix.
    """
    if super == "duper":
        from super_duper import duper as super

    class HasDuperAttr:
        def __init__(self):
            self._super = 10
            return

        def _find(self):
            return 42

    class UsesDuperAttr(HasDuperAttr):
        _super = 20

        def _find(self, val):
            return super()._find() * val

        def get_super(self):
            super()._super += 3
            return super()._super

    uda = UsesDuperAttr()
    res = uda._find(2)
    assert res == 84
    with expect:
        res = uda.get_super()
        assert res == 13
    return
