# Super Duper
A drop-in replacement patch for Python's `super` builtin.

## Why?
Python does not support setting (or deleting) properties in a base class. So the following will not work, throwing `AttributeError`:

``` python
class Base:
    def __init__(self):
        self._x = 0
        self._y = 10
        return
    @property
    def x(self):
        return self._x
    @x.setter
    def x(self, v):
        self._x += 3
        return

class Use(Base):
    @property
    def x(self):
        return self._x
    @x.setter
    def x(self, v):
        super().x += v * self.y
        return
    @property
    def y(self):
        return self._y

use = Use()
use.x = 3
```
One workaround is to access the property's internals, for example replace `super().x += v * self.y` with `super(self.__class__, self.__class__).x.fset(self, super().x + v * self.y)`, but this is very verbose and error-prone. The feature is particularly very useful for projects with a high usage of inheritance and properties, as it allows subclasses to use or update related attributes when another is updated.

As it stands (on 2022-10-26), there has been an [open issue](https://github.com/python/cpython/issues/59170) since 2012, and an associated [pull request](https://github.com/python/cpython/pull/29950) since last year. The PR has gained some recent activity, but it is an unknown when it will finally be merged. Hence the reason for this patch. It is based on [another workaround](https://bugs.python.org/file50057/duper.py), but allows for zero-arguments instanciation as introduced in [PEP 3135](https://peps.python.org/pep-3135/). It aims to behave as much as the builtin `super` as is practical.

## Install
- ~~Super Duper is available on PyPI:~~ (a naming issue needs to be resolved)
  + ~~Run `pip install super-duper`~~
- Super Duper can be installed directly from GitLab:
  + Ensure [poetry](https://python-poetry.org/docs/#installation) is installed.
  + Run `poetry add git+https://gitlab.com/skeledrew/super-duper.git`

## Usage
- The primary mode of use is to replace the builtin version via `install()`. Add to, for example, `some_project/__init__.py`:
  ```python
  from super_duper import install
  install()
  ```
  + This makes it available everywhere in the Python session.
  + The patch can be removed by importing and running `uninstall()`, and install status checked with `check_duper_is_installed()`.
- Alternatively, it can be used at the module level:
  ``` python
  from super_duper import duper as super
  ...
  super()
  ```
  + This method makes it available only in modules where it is explicitly imported.
  + Keep in mind that it **MUST** be used under the "super" name, or else behavior is undefined, particularly for the zero-arguments invokation (see PEP 3135).

## Notes
- Super Duper uses warnings to present certain information to the user, such as install and uninstall actions.
- If the issue targeted by Super Duper is fixed, builtin installation will be automatically skipped.

## Troubleshooting
In case of problems where the fault may lie with Super Duper, it can be checked with a test similar to the following:
``` python
import pytest
import super_duper as sd

@pytest.mark.parametrize("version", ["original", "patched"])
def test_breaking_code(version):
    assert not sd.check_duper_is_installed()

    if version == "patched":
        sd.install()
    run_breaking_code()
    assert 1
    sd.uninstall()
    return
```
This will run with the original and then the patched `super`. If the test fails on "patched" but not on "original", then Super Duper is likely at fault. Please file an issue.

## License
MIT
