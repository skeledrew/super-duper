"""A drop-in replacement for the `super()` builtin for property set and delete.

References:
  - https://bugs.python.org/file50057/duper.py

Usage:
  from super_duper import install
  install()
  ...
  super()

NOTE:
  - The import MUST be named "super", else things will break.
"""


__all__ = [
    "__version__",
    "check_duper_is_installed",
    "duper",
    "install",
    "uninstall",
]
__version__ = "0.1.0"


import builtins
import inspect
from warnings import warn

INSTALLED = False


class NULL:
    pass


# def redirect(fn):
#     """Redirect calls hitting implemented attrs to the super."""
#     _is_initd = False
#     _fname = fn.__name__

#     def _redirect(oduper, *args, **kwds):
#         nonlocal _fname, _is_initd

#         # breakpoint()

#         try:
#             osuper = object.__getattribute__(oduper, "osuper")

#         except AttributeError:
#             osuper = None

#         if not osuper:
#             rv = fn(oduper, *args, **kwds)

#         else:
#             rv = getattr(osuper, _fname)(*args, **kwds)

#         if _fname == "__init__":
#             _is_initd = True
#         return rv
#     return _redirect


class duper:
    """Super replacement which allows property setting & deletion.

    Works with multiple inheritance.
    NOTE: Assumes that the caller uses standard arg names.
    """

    def __init__(self, type_=NULL, obj_or_type=NULL(), /):  # noqa
        object.__setattr__(self, "_in_duper", True)
        if not isinstance(type_, type):
            t_name = type(type_).__name__
            raise TypeError(f"super() argument 1 must be type, not {t_name}")

        if not isinstance(obj_or_type, NULL):
            err = "super(type, obj): obj must be an instance or subtype of type"  # noqa: E501
            if isinstance(obj_or_type, type) and not issubclass(obj_or_type, type_):
                raise TypeError(err)

            if not isinstance(obj_or_type, type) and not isinstance(obj_or_type, type_):
                raise TypeError(err)
        nsuper = None

        if type_ is NULL:
            # no args given; get both from caller
            frame = inspect.currentframe().f_back
            f_locs = frame.f_locals

            if "__class__" not in f_locs:
                if "super" not in f_locs:
                    raise SystemError("`duper` must be imported as `super`")
                raise SystemError("`super` must be used in an instance or class method")
            type_ = f_locs["__class__"]
            fname = frame.f_code.co_name
            del frame
            attr = getattr(type_, fname)

            if isinstance(attr, property):
                func = attr.fget

            elif callable(attr):
                func = attr

            else:
                raise AttributeError("only property and callable attributes supported")
            args_spec = inspect.getfullargspec(func)._asdict()

            if not args_spec["args"]:
                raise TypeError(
                    f"{type_.__name__}.{fname}() takes 0 positional arguments but 1+ were given"
                )
            arg1_name = args_spec["args"][0]
            obj_or_type = f_locs[arg1_name]
        args = (
            (type_, obj_or_type)
            if type_ is not NULL and not isinstance(obj_or_type, NULL)
            else (type_,)
        )
        nsuper = object.__getattribute__(self, "_super")(*args)
        object.__setattr__(self, "osuper", nsuper)
        object.__setattr__(self, "_in_duper", False)
        return

    def __repr__(self):
        return repr(object.__getattribute__(self, "osuper"))

    def __str__(self):
        return str(object.__getattribute__(self, "osuper"))

    def _find(self, name):
        osuper = object.__getattribute__(self, "osuper")
        if name != "__class__":
            mro = iter(osuper.__self_class__.__mro__)
            for cls in mro:
                if cls == osuper.__thisclass__:
                    break
            for cls in mro:
                if isinstance(cls, type):
                    try:
                        return object.__getattribute__(cls, name)
                    except AttributeError:
                        pass
        return None

    def _super(self, *args):
        return (super if not INSTALLED else builtins._super)(*args)

    def __getattribute__(self, name):
        in_duper = object.__getattribute__(self, "_in_duper")

        if not in_duper:
            getattr_ = object.__getattribute__(self, "__getattr__")
            return getattr_(name)
        return object.__getattribute__(self, name)

    def __getattr__(self, name):
        osuper = object.__getattribute__(self, "osuper")

        if True:
            # NOTE: bypass the (buggy) specialization code; may revisit if
            #         needed to complete *_attr_collision
            return getattr(osuper, name)

        if osuper.__self_class__ is not None:

            try:
                getattr(osuper, name)
                target = osuper

            except AttributeError:
                if name in dir(osuper):
                    target = osuper.__self__

                else:
                    raise

        else:
            target = osuper.__thisclass__
        return getattr(target, name)

    def __setattr__(self, name, value):
        object.__setattr__(self, "_in_duper", True)
        osuper = object.__getattribute__(self, "osuper")
        desc = object.__getattribute__(self, "_find")(name)
        if hasattr(desc, "__set__"):
            return desc.__set__(osuper.__self__, value)
        try:
            return setattr(osuper, name, value)

        except AttributeError as exc:
            # setattr(osuper.__self__, name, value)
            warn(f"{repr(exc)} was ignored")

        finally:
            object.__setattr__(self, "_in_duper", False)
        return

    def __delattr__(self, name):
        osuper = object.__getattribute__(self, "osuper")
        desc = object.__getattribute__(self, "_find")(name)
        if hasattr(desc, "__delete__"):
            return desc.__delete__(osuper.__self__)
        return delattr(osuper, name)


def check_duper_is_installed():  # noqa: D103
    return INSTALLED


def install():
    """Replace `super` with `duper` in `builtins`."""
    # TODO: check if patch is fixed first
    global INSTALLED

    if INSTALLED:
        return
    tc = (
        str(getattr(super, "__thisclass__", ""))
        == "<member '__thisclass__' of 'super' objects>"
    )
    sc = (
        str(getattr(super, "__self_class__", ""))
        == "<member '__self_class__' of 'super' objects>"
    )

    if hasattr(builtins, "_super") or not (tc and sc):
        warn("not applying super patch as something else may have already")
        return

    class A:
        @property
        def x(self):
            return

        @x.setter
        def x(self, v):
            return

    class B:
        @property
        def x(self):
            return

        @x.setter
        def x(self, v):
            super().x = v
            return

    try:
        B().x = 0

    except AttributeError:
        pass

    else:
        warn("not patching super; property setting on base class seems fixed 🎉")
        return
    builtins._super = super
    assert _super  # noqa: F821
    builtins.super = duper
    INSTALLED = True
    warn("patched `super` with `duper` in `builtins`")
    return


def uninstall():
    """Restore `super` in `builtins`."""
    global INSTALLED

    if not INSTALLED:
        return
    assert hasattr(builtins, "_super")
    builtins.super = builtins._super
    del builtins._super
    INSTALLED = False
    warn("`duper` patch removed from `builtins`")
    return
